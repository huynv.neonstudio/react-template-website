import React from "react";
import constants from './constants'
import HomeNavigator from './modules/home'
import AboutPage from './modules/about'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

const AppNavigator = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={constants.pages.ROOT_PAGE}>      <HomeNavigator /> </Route>
        <Route  path={constants.pages.HOME_PAGE}>      <HomeNavigator /> </Route>
        <Route  path={constants.pages.ABOUT_PAGE}>     <AboutPage /> </Route>
      </Switch>
    </Router>
  );
}
export default AppNavigator;
