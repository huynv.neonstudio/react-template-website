import * as apiLinks from '../constants/apiLinks'
import request from './makeRequest'
const api ={
    getDemoData         :      (data => request.get(apiLinks.INITIAL,data) ),
    getUserInfo         :      (data => request.post(apiLinks.API_USER_GET_INFO,data) ),
    login               :      (data => request.post(apiLinks.API_USER_LOGIN,data) ),
    }  
export default api