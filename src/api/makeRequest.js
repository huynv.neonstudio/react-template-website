import store from '../store'
const request = {
    get: async (url,data) => {
        //const regimereducer = store.getState().regimeReducer
        //const userReducer = store.getState().userReducer
        //const device_info =  await AsyncStorage.getItem('tokenFB');
        //data.token = userReducer.token
        // if (data.key) {
        //     data.key = data.key
        // } else {
        //     data.key = API_KEY
        // }
        // data.device_info = device_info
        // data.device_type = DEVICE_TYPE
        // data.app_type = 'react-native'
        url = new URL(url)
        Object.keys(data).forEach(key => url.searchParams.append(key, data[key]))
        //console.log("url",url)
        let result = await fetch(url)
        if (result.ok) {
            return result.json()
        } else {
            return {
                status: false,
                data: result.json(),
                message: "connect server failed"
            }
        }
    },
    post: async (url, data) => {
         const userReducer = store.getState().userReducer
         let token = ""
         let userAuth = localStorage.getItem("user_auth")
         if(userAuth)
            token = JSON.parse(userAuth).token
        let result = await fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                token : token
            },
            body: JSON.stringify(data)
        })
         console.log(result)
        if (result.ok) {
            return result.json()
        } else {
            return {
                status: false,
                data: {},
                message: "connect server failed"
            }
        }
    },
    uploadFile: (url, data, token) => {
        console.log(data)
        console.log(url, data)
        return new Promise(function (resolve, reject) {
            fetch(url, {
                method: 'POST',
                body: data
            }).then((response) => {
                console.log(response)
                try {
                    return response.json()
                } catch (err) {
                    reject({ err: 2, msg: 'Phiên làm việc hết hạn' })
                }
            }).then((data) => {
                console.info(data);
                resolve(data);
            }).catch((err) => {
                console.log(err)
                reject({ err: 1, msg: 'Vui lòng kiểm tra kết nối mạng' })
            })
        });

    },
    
}
export default request;
