export const INITIAL = 'https://jsonplaceholder.typicode.com/todos';//https://api.vtvcab.vn:8889/

export const HOST = "http://localhost:3001"
export const API  = HOST + "/api"
export const API_USER = API + "/user"
export const API_USER_LOGIN = API_USER + "/login"
export const API_USER_GET_INFO = API_USER + "/getUserInfo"