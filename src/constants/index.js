const constants = {
    pages         : require('./pages'),
    apiLinks      : require('./apiLinks'),
    logos         : require('./logos')
  }
export default constants
