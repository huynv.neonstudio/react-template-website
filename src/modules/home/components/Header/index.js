import React, { useState } from "react";
import constants from '../../../../constants'
import "./header.css";
import { useHistory } from "react-router-dom";
import {
  Link,
} from "react-router-dom";

const Header = () => {
  let history = useHistory();
  const changePage = event => {
    history.push({
        pathname: '/about/',
        // search: '?query=abc',
    });
 };
  const [click, setClick] = useState(false);
  const closeMobileMenu = () => setClick(false);
  return (
    <div className="header">
      <div className="logo-nav">
        <div className="logo-container">
          <a href="#">
            {/* <Logo className="logo" /> */}
            <img style={{width : 80, height : 50}} src={constants.logos.uriLogoVTV.default} ></img>
          </a>
        </div>
      </div>
      <ul className="signin-up">
        <li className="sign-in" onClick={closeMobileMenu}>
          <a href="#">SIGN-IN</a>
        </li>
        <li onClick={closeMobileMenu}>
          <a href="" className="signup-btn">
            SIGN-UP
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Header;