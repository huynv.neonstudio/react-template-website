

import './styles.css';
const SlideLefBar = () => {
  return (<div className='slideLef'>
    <aside>
      <ul>
        <li>
          <div className="icons">
            <i className="fa fa-heart"> </i>
          </div>
         
          <div className="linktitle">Favorite
          </div>
        </li>
        <li><div className="icons"><i className="fa fa-align-justify"></i></div><div className="linktitle">Channels</div></li>
        <li><div className="icons"><i className="fa fa-group"></i></div><div className="linktitle">My Group</div></li>
        <li><div className="icons"><i className="fa fa-user"></i></div><div className="linktitle">My Profile</div></li>
      </ul>
    </aside>
  </div>)
}
export default SlideLefBar