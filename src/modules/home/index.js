import React from "react";
import ReactDOM from 'react-dom';
import constants from '../../constants'
import HomePage from './pages/HomePage'
import {
  Route,
  Redirect,
} from "react-router-dom";

const HomeNavigator = () => {
  return (
    <div>
      <Route path={constants.pages.ROOT_PAGE}>  <HomePage /></Route>
      <Redirect from={constants.pages.HOME_PAGE} to={constants.pages.ROOT_PAGE} exact /> 
    </div>
  );
}
export default HomeNavigator;

