
import React from 'react';
import Header from '../../components/Header'
import SlideLefBar from '../../components/SlideLefBar'
import api from '../../../../api'
import { useSelector, useDispatch } from "react-redux";
import { updateUserInfo } from '../../../../store/actions/updateUserInfo';
import "./home.css"
const { useEffect, useState } = React;
const HomePage = () => {
  const dispatch = useDispatch();
  // Lấy thông tin từ redux store biến này có thể truy cập từ tất cả các màn hình 
  const userInfo = useSelector(state => state.userReducer)
  const [resultGetInfo, setresultGetInfo] = useState({});
  const getUserInfo = async () => {
    const resultGetInfo = await api.getUserInfo()
    if(resultGetInfo.isSuccess)
    {
      console.log(resultGetInfo)
      setresultGetInfo(resultGetInfo.data)
      dispatch(updateUserInfo({
        username: resultGetInfo.data.username
      }))
    }
    
  }
  const [username, setusername] = useState("");
  const [password, setpassword] = useState("");
  const [userLogin, setuserLogin] = useState({});
  const Login = async () => {
    const resultLogin = await api.login({
      username,
      password
    })
    setuserLogin(resultLogin)
    if (resultLogin.isSuccess) {
      localStorage.setItem("user_auth", JSON.stringify(resultLogin.data))
    }
    console.log(resultLogin)
  }
  // hàm gọi khi bắt đầu vào màn hình 
  useEffect(() => {
    
  }, [])

  return (<div>
    <Header></Header>
    <button onClick={getUserInfo}><span>Lấy thông tin user</span> </button> <label>{JSON.stringify(resultGetInfo)}</label> <br />
    <label>Nhập username </label>
    <input onChange={(e) => { setusername(e.target.value) }} />
    <br />
    <label>Nhập password </label>
    <input onChange={(e) => { setpassword(e.target.value) }} />

    <br />
    <button onClick={Login}><span>Đăng nhập</span> </button>

    <br />
    <label>Kết quả đăng nhập {JSON.stringify(userLogin)}</label>
    <br/>
    <span>Dữ liệu lấy ra từ redux</span>
    <pre>{JSON.stringify(userInfo)}</pre>
    <SlideLefBar></SlideLefBar>
  </div>)
}
export default HomePage