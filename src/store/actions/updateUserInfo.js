import * as types from '../constants/actionTypes';
export const updateUserInfo=(data)=>{
        //alert(JSON.stringify(data))
    return {
        type: types.UPDATE_USER_STATE,
        username: data.username,
        fullName: data.fullName,
        avatar  : data.avatar
       
    };
}
export const updateUserAvatar=(data)=>{
     return {
        type: types.UPDATE_USER_AVATAR,
        avatar: data.avatar,
    };
}
