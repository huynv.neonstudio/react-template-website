
import { useReducer } from 'react'
import { UPDATE_USER_STATE ,UPDATE_USER_AVATAR} from '../constants/actionTypes'

const initialState = {
 username : '',
 fullName : '',
 avatar   : '',
 token    : ''
}
const userReducer = (state = initialState, actions) => {
    console.log("actions",actions)
    switch (actions.type) {
        case UPDATE_USER_STATE:
            return {
                ...state,
                username : actions.username,
                fullName : actions.fullName
               
            }
            case UPDATE_USER_AVATAR:
                return {
                    ...state,
                    avatar: actions.avatar,
                }
        default: return state
    }
}
export default userReducer;